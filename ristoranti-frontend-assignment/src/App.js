import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from "@apollo/react-hooks";
import CreateRestaurantForm from './CreateRestaurantForm'
import RestaurantsList from './RestaurantsList'

const apolloClient = new ApolloClient({
  uri: 'http://localhost:3001/graphql',
});

const App = () => {
  return (
    <ApolloProvider client={apolloClient}>
      <div style={{margin: '30px', textAlign: "center"}}>
        <img
          alt={"Ristoranti.it logo"}
          src={'https://www.ristoranti.it/images/ristoranti-logo.png'}
        />
        <h4>Ristoranti.it Assignment</h4>
        <h2>#CreateRestaurantForm</h2>
        <CreateRestaurantForm />
        <h2>#RestaurantList</h2>
        <RestaurantsList />
      </div>
    </ApolloProvider>
  )
}

export default App;
