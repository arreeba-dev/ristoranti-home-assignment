# Ristoranti-Backend-Assignment

## Getting Started

### Prerequisites

To run the project you need to have [node](https://nodejs.org/) installed.

### Running: 

Install dependencies:
```
yarn
```
Run:
```
yarn start
```
## Notes

To start the project with Node.js, you must first complete it with the missing parts.

## Stack

* [Node.js](https://nodejs.org/it/) - Open-source, cross-platform, JavaScript runtime environment.
* [Express.js](https://expressjs.com/it/) -  Web application framework for Node.js.
* [Apollo Server](https://www.apollographql.com/docs/apollo-server/) - library that helps you connect a GraphQL schema to an HTTP server in Node.
