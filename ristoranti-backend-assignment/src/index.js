import express from 'express';
import { ApolloServer } from 'apollo-server-express';

const app = express();

// const schema = ...
// const resolvers = ...

const server = new ApolloServer({
  typeDefs: schema,
  resolvers,
});

server.applyMiddleware({ app, path: '/graphql' });

app.listen(3001, () => {
  console.log('GraphQl playground on http://localhost:3001/graphql');
});
