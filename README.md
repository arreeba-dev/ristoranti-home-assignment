# Ristoranti.it assignment

For this assignment what you will build is a simple page and a server app for querying and filtering a dataset of restaurants.  
The restaurants dataset is harcoded inside the project.

It is not important to have an extremely beautiful UI in terms of design, but we need to have a consistent and working interface allowing the users to work with data without frictions.

## Introduction

The server app will be powered by `javascript`, `Node.js`, `graphQL` and [`apollo-server`](https://www.apollographql.com/docs/apollo-server/).
The client app will be powered by `javascript`, `react` and [`apollo-client`](https://www.apollographql.com/docs/react/).
You can add other libraries if needed.

This project is powered by `yarn`.
Once cloned this project can be installed running _`yarn`_ in the two main directories. You can run both your frontend and your backend apps with _`yarn start`_ in the relative directory.
The backend server runs on `localhost:3001`, while the frontend app runs on `localhost:3000`.

## Tasks

### Server side

Implement `getRestaurants` query and `createRestaurants` mutation, exposed with graphQL.

The `getRestaurants` query will be used by the frontend app to list all the restaurants.

The `createRestaurants` mutation will be used to add records in the dataset.

### Client side

Implement `CreateRestaurantForm` and `RestaurantsList` components.

`CreateRestaurantForm` is the form to add records in the restaurants dataset.

`RestaurantsList` is the view that contains all the restaurants and their data.

## Evaluation

Our goal is to see if you master javascript and if you are able to understand the basic functionality of our main architecture (React, Node.js and graphQL).


